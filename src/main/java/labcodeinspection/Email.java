package labcodeinspection;
import java.util.Locale;

/**
 * The Email class represents an email account for a user
 */
public class Email {

	private String m_firstName;//NOPMD this field will be manipulated later
	private final String lastName; //NOPMD Last name of the user
	private String password; //NOPMD Password for the email
	private String department; //NOPMD Department of the user
	private final int defaultPasswordLength = 8; //NOPMD Default length for the password
	private String email; //NOPMD Email address generated

	/**
	 * Constructor for Email initializes with user's first and last name.
	 * @param firstName The first name of the user
	 * @param lastName The last name of the user
	 */
	public Email(final String firstName, final String lastName) {
		this.m_firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * Displays user's email information.
	 */
	public void showInfo() {
		System.out.println("\nFIRST NAME= " + m_firstName + "\nLAST NAME= " + lastName);
		System.out.println("DEPARMENT= " + department + "\nEMAIL= " + email + "\nPASSWORD= " + password);
	}

	/**
	 * Sets the department based on user choice.
	 * @param depChoice Department choice input by the user
	 */
	public void setDeparment(final int depChoice) {
		switch (depChoice) {
		case 1:
			this.department = "sales";
			break;
		case 2:
			this.department = "dev";
			break;
		case 3:
			this.department = "acct";
			break;
		default:
            System.out.println("Invalid choice");
            break;
		}
		
	}

	private String randomPassword(final int length) {
		final String set = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890#$&@*";
		final char[] password = new char[length];
		for (int i = 0; i < length; i++) {
			final int rand = (int) (Math.random() * set.length());
			password[i] = set.charAt(rand);
		}
		return new String(password);
	}

	/**
	 * Generates email address using first name, last name, and department.
	 */
	public void generateEmail() {
		this.password = this.randomPassword(this.defaultPasswordLength);
		this.email = (this.m_firstName.toLowerCase(Locale.ENGLISH) + this.lastName.toLowerCase(Locale.ENGLISH) + "@" + this.department
	            + ".espol.edu.ec").toLowerCase(Locale.ENGLISH);
	}
}
