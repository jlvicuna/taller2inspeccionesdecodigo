package labcodeinspection;

import java.util.Scanner;


/**
 * Main class for Email application
 */
@SuppressWarnings("PMD.UseUtilityClass")
public class EmailApp {

	/**
     * Main method to run the EmailApp.
     * @param args command line arguments (not used)
     */
	public static void main(final String[] args) {
		final Scanner sc = new Scanner(System.in);//NOPMD "sc" es el nombre por defecto que damos al inicializar un Scanner

		System.out.print("Enter your first name: ");
		final String firstName = sc.nextLine();

		System.out.print("Enter your last name: ");
		final String lastName = sc.nextLine();

		System.out.print("\nDEPARTMENT CODE\n1. for sales\n2. for Development\n3. for accounting\nEnter code: ");

		final int depChoice = sc.nextInt();
		sc.close();

		final Email email = new Email(firstName, lastName);
		email.setDeparment(depChoice);
		email.generateEmail();
		email.showInfo();
	}
	
}
